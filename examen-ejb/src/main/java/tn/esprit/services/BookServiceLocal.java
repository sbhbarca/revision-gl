package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Book;

@Local
public interface BookServiceLocal {

	public void addBook(Book b);
	public void updateBook(Book b);
	public void deleteBook(Integer id);
	public Book getBookById(Integer id);
	public List<Book> getAllBook();
}
