package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Book;

/**
 * Session Bean implementation class BookService
 */
@Stateless
public class BookService implements BookServiceRemote, BookServiceLocal {

    /**
     * Default constructor. 
     */
    public BookService() {
        // TODO Auto-generated constructor stub
    }

    @PersistenceContext
    EntityManager em;
    
	@Override
	public void addBook(Book b) {
		// TODO Auto-generated method stub
		em.persist(b);
	}

	@Override
	public void updateBook(Book b) {
		// TODO Auto-generated method stub
		em.merge(b);
	}

	@Override
	public void deleteBook(Integer id) {
		// TODO Auto-generated method stub
		em.remove(em.find(Book.class, id));
	}

	@Override
	public Book getBookById(Integer id) {
		// TODO Auto-generated method stub
		return em.find(Book.class, id);
	}

	@Override
	public List<Book> getAllBook() {
		// TODO Auto-generated method stub
		TypedQuery<Book> req = em.createQuery("select b from Book b", Book.class);
		
		return req.getResultList();
	}

}
