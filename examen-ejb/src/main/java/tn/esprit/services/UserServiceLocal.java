package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.User;

@Local
public interface UserServiceLocal {

	public void addUser(User u);
	public void updateUser(User u);
	public void deleteUser(Integer id);
	public User getUserById(Integer id);
	public List<User> getAllUser();
	public User authentifier(String login, String pwd);
}
