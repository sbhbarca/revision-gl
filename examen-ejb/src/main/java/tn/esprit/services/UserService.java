package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Book;
import tn.esprit.entities.User;

/**
 * Session Bean implementation class UserService
 */
@Stateless
public class UserService implements UserServiceRemote, UserServiceLocal {

    /**
     * Default constructor. 
     */
    public UserService() {
        // TODO Auto-generated constructor stub
    }

    @PersistenceContext
    EntityManager em;
    
    
	@Override
	public void addUser(User u) {
		// TODO Auto-generated method stub
		em.persist(u);
	}

	@Override
	public void updateUser(User u) {
		// TODO Auto-generated method stub
		em.merge(u);
	}

	@Override
	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		em.remove(em.find(User.class, id));
	}

	@Override
	public User getUserById(Integer id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		TypedQuery<User> req = em.createQuery("select b from User b", User.class);
		
		return req.getResultList();
	}

	@Override
	public User authentifier(String login, String pwd) {
		// TODO Auto-generated method stub
		TypedQuery<User> req = em.createQuery("select b from User b where b.login like :log and b.password like :password", User.class);
		req.setParameter("log", login).setParameter("password", pwd);
		try {
			return req.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("User not found");
		}
		return null;
	}

}
