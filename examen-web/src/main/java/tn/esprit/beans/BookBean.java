package tn.esprit.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.entities.Book;
import tn.esprit.services.BookServiceLocal;

@ManagedBean
@SessionScoped
public class BookBean {


	@EJB
	BookServiceLocal bs;
	
	
	@PostConstruct
	public void init(){
		books = bs.getAllBook();
		book = new Book();
	}
	
	public String back1(){
		return "/pages/addlivre?faces-redirect=true";
	}
	public String back2(){
		return "/pages/addlivre1?faces-redirect=true";
	}
	public String cancel(){
		return "/pages/welcome?faces-redirect=true";
	}
	public String next1(){
		return "/pages/addlivre2?faces-redirect=true";
	}
	public String add(){
		
		bs.addBook(book);
		books.add(book);
		book = new  Book();
		return "welcome?faces-redirect=true";
	}
	
	private Book book;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}


	private List<Book> books;
	
	

}
