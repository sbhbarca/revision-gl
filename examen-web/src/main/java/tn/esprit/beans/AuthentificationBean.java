package tn.esprit.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import tn.esprit.entities.User;
import tn.esprit.services.UserServiceLocal;

@ManagedBean 
@SessionScoped
public class AuthentificationBean {

	@EJB
	UserServiceLocal service;
	
	private User user;
	private String login;
	private String password;
	private String resultat="";

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String doLogin(){
		String navigateTo= null;
		user = service.authentifier(login, password);
		if (user != null) {
			resultat = user.getLogin();
			navigateTo= "/pages/welcome?faces-redirect=true";
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad Connexion"));
		}
		return navigateTo;
		
	}
	
	public String logOut(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login?faces-redirect=true";
	}

	public String getResultat() {
		return resultat;
	}

	public void setResultat(String resultat) {
		this.resultat = resultat;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
