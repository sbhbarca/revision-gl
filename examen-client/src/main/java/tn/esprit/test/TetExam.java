package tn.esprit.test;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Book;
import tn.esprit.entities.User;
import tn.esprit.services.BookServiceRemote;
import tn.esprit.services.UserServiceRemote;

public class TetExam {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		InitialContext ctx = new InitialContext();
		
		UserServiceRemote us = (UserServiceRemote) ctx.lookup("examen-ear/examen-ejb/UserService!tn.esprit.services.UserServiceRemote");
		BookServiceRemote bs = (BookServiceRemote) ctx.lookup("examen-ear/examen-ejb/BookService!tn.esprit.services.BookServiceRemote");
		
		User u = new User();
		u.setLogin("ahmed");
		u.setPassword("esprit");
		u.setEmail("ahmed@esprit.tn");
		
		us.addUser(u);
		
		Book b = new Book();
		b.setTitle("LNAI");
		b.setPrice(60);
		b.setDescription("Computer Science");
		b.setIllustrations(false);
		b.setNbrOfpages(100);
		
		Book b1 = new Book();
		b1.setTitle("Povoir illimit�");
		b1.setPrice(20);
		b1.setDescription("Livre de NLP");
		b1.setIllustrations(true);
		b1.setNbrOfpages(393);
		
		bs.addBook(b);
		bs.addBook(b1);
		
		
	}

}
